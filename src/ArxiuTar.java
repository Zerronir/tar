class ArxiuTar {

    // Cream les variables locals
    String nom;
    String nomArxiu;
    String tipus;
    int tamany;
    byte[] capçalera;
    byte[] contingut;

    ArxiuTar(){}

    public void build(){
        // Adjutsam el tamany de l'arxiu
        tamany = contingut.length + capçalera.length;

        // Separam l'extensió de l'arxiu del nom de l'arxiu
        StringBuilder separador = new StringBuilder();
        separador.append(nomArxiu);
        // Ho separam a l'altura del .
        int temporary = separador.indexOf(".");
        nom = separador.substring(0, temporary-1);
        tipus = separador.substring(temporary + 1);
    }

    // Cream els setters per a nom d'arxiu, capçalera y contingut
    public void setNomArxiu(String nomArxiu) {
        this.nomArxiu = nomArxiu;
    }

    public void setCapçalera(byte[] capçalera) {
        this.capçalera = capçalera;
    }

    public void setContingut(byte[] contingut) {
        this.contingut = contingut;
    }
}
