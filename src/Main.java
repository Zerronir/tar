import java.io.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
        // Iniciam el programa
        boolean inici = true;
        // Cream un escaner per a les opcions que podrà fer servir l'usuari
        Scanner opt = new Scanner(System.in);

        /*
         *
         * Tenim 3 opcions que hem de implementar a part de poder carregar l'arxiu a la memoria:
         *
         * 1 - Carregar el fitxer .tar a la memòria
         * 2 - LListar els arxius del fitxer .tar
         * 3 - Extreure els arxius del fitxer .tar
         *
         * */

        System.out.println("El programa està iniciat, si vols sortir has de escriure sortir, per continuar escriu 'next'");
        String exit = opt.next();
        if(exit.equals("Sortir")){
            inici = false;
        }else{
            // Cream una variable per assignar que el programa està correguent
            boolean executant = true;

            while(executant) {
                // Cream l'arxiu
                Tar t = new Tar("src/archive2.tar");
                if(t.trobat){
                    // Demanam a l'usuari la opció que vol executar
                    System.out.println("Digues que comanda vols executar primer:\n1 - Carregar\n2 - LListar arxius\n3 - Extreure\n4 - Sortir");
                    int setOpt = opt.nextInt();
                    switch(setOpt){
                        case 1:
                            // Executam la comanda de càrrega de l'arxiu a la memòria
                            load(t);
                            break;

                        // LListam els arxius de dintre del fitxer TAR
                        case 2:
                            // Comprobam que l'arxiu estigui carregat a la memòria, si no ho està ho carregam
                            if(t.carregat){
                                System.out.println("arxiu carregat");
                                System.out.println("---------------");
                                // Executam la funció per a llistar els arxius del fitxer tar
                                list(t);
                                System.out.println("---------------");

                            }else{
                                System.out.println("He de carregar l'arxiu primer");
                                load(t);
                                System.out.println("---------------");
                                // Executam la funció per a llistar els arxius del fitxer tar
                                list(t);
                                System.out.println("---------------");
                            }
                            break;

                        // Extreurem els arxius que seleccionem a la ruta on l'usuari diga
                        case 3:
                            // Carregam l'arxiu a la memòria
                            if(!t.carregat){

                                load(t);

                                System.out.println("Digues quin arxiu vols extreure: ");
                                String[] llistaArxius = t.list();
                                for (int i = 0; i < llistaArxius.length; i++) {
                                    System.out.println(i+1 + " -- " + llistaArxius[i]);
                                }
                                int optExt = opt.nextInt();
                                System.out.println("On vols extreure l'arxiu?");
                                System.out.println("Escriu: <<local>> per extreure dintre d'aquesta carpeta o escriu la ruta on el vols extreure");
                                // Emmagatzeman l'input de l'usuari d'on vol extreure l'arxiu
                                String path = opt.next();
                                // Executam la comanda de extracció
                                extract(t, path, optExt);

                            }else{
                                System.out.println("Digues quin arxiu vols extreure: ");
                                String[] llistaArxius = t.list();
                                int i;
                                for (i = 0; i < llistaArxius.length; i++) {
                                    System.out.println(i+1 + " -- " + llistaArxius[i]);
                                }
                                int optExt = opt.nextInt();
                                if(optExt != i+1){
                                    System.out.println("On vols extreure l'arxiu?");
                                    System.out.println("Escriu: <<local>> per extreure dintre d'aquesta carpeta o escriu la ruta absoluta on el vols extreure");
                                    // Emmagatzeman l'input de l'usuari d'on vol extreure l'arxiu
                                    String path = opt.next();
                                    // Executam la comanda de extracció
                                    extract(t, path, optExt);
                                }
                            }
                            break;

                        // Tancarem el bucle
                        case 4:
                            executant = false;
                            break;
                    }

                    break;
                }
            }

        }



    }

    // Funció per a carregar l'arxiu a la memòria
    public static void load(Tar t) throws IOException {
       if(t.carregat){
           System.out.println("L'arxiu ja es carregat a la memòria");
       }else{
           t.expand();
       }
    }

    // Funció per a llistar els arxius
    public static void list(Tar t){
        // Cream una array amb la llista dels arxius que estàn a l'arxiu .tar
        String[] nameList = t.list();
        for (String nomArxiu : nameList){
            // Feim un print de cada arxiu
            System.out.println("Arxiu ---> " + nomArxiu);
        }
    }

    // Funció per a extreure els arxius
    public static void extract(Tar t, String loc, int opt) throws FileNotFoundException {
        ArxiuTar at = t.arxiusList.get(opt + 1);

        if(loc.equals("local") || loc.equals("Local")){

            File f = new File ("src/" + at.nomArxiu);
            OutputStream outputFirst = new FileOutputStream(f);

            try{
                // Extreiem l'arxiu
                outputFirst.write(at.contingut);
                outputFirst.close();
                System.out.println("L'arxiu s'ha extret correctament");
            }catch (Exception e){
                System.out.println("No he pogut extreure l'arxiu");
            }

        }else{
            // Extreiem l'arxiu a la ruta que ens ha donat l'usuari
            try{
                File f = new File(loc + "\\" + at.nomArxiu);
                OutputStream output = new FileOutputStream(f);

                // Extreiem l'arxiu
                output.write(at.contingut);
                output.close();
                System.out.println("L'arxiu s'ha extret correctament");

            }catch(Exception e){
                System.out.println("No he pogut extreure l'arxiu");
            }


        }


    }

}

