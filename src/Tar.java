import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Tar {

    List<ArxiuTar> arxiusList = new ArrayList<>();
    String locArxiu;
    byte[] arxiuSencer;
    byte[] arxiu;
    int blockSize = 512;
    int byteActual = 0;
    byte[] capçaleraTar = new byte[blockSize];
    int size;
    InputStream lector;
    int ompleTamany;
    boolean carregat = false;
    boolean trobat = false;
    boolean totTrobat = false;

    // Cream el constructor de la classe Tar
    public Tar(String locarxiu) throws FileNotFoundException {
        this.locArxiu = locarxiu;
        // Cream un arxiu Tar amb la ruta que hem passat
        try{
            lector = new FileInputStream(locArxiu);
            System.out.println("He trobat l'arxiu: " + locarxiu);
            trobat = true;
        }catch (FileNotFoundException fnf){
            System.err.println(fnf);
        }

    }

    void adjustaTamany(byte[] capçaleraTar){

        StringBuilder tamany = new StringBuilder();


        for (int i = 0; i < 11; i++) {
            tamany.append(capçaleraTar[124 + i] - 48);
        }

        int temporarySize = Integer.parseInt(tamany.toString(), 8);
        this.size = temporarySize;
        while(temporarySize % 512 != 0){
            temporarySize++;
        }

        this.ompleTamany = temporarySize;

    }


    // Torna un array amb la llista de fitxers que hi ha dins el TAR
    public String[] list() {

        // Cream una array per a emmagatzemar el nom de cada ficher que l'arxiu .tar conté
        String[] resultat = new String[arxiusList.size()];

        // Cream un bucle per a crear una array amb el nom dels arxius que conté el .tar
        for (int i = 0; i < arxiusList.size(); i++) {
            resultat[i] = arxiusList.get(i).nomArxiu;
        }

        // Tornam el resultat
        return resultat;
    }

    // Torna un array de bytes amb el contingut del fitxer que té per nom
    // igual a l'String «name» que passem per paràmetre
    public byte[] getBytes(String name) {

        // Cream un bucle per comprobar l'integritat de l'arxiu
        for(ArxiuTar at : arxiusList){

            // Si el nom coincideix retornarem el contingut de la variable at
            if(at.nomArxiu.equals(name))
                return at.contingut;
        }
        // Si l'arxiu no existeix retornarem un null
        return null;
    }

    // Expandeix el fitxer TAR dins la memòria
    public void expand() throws IOException {

        arxiuSencer = lector.readAllBytes();

        while(!totTrobat){

            // Cream un arxiu temporal que emplearà una copia de l'original
            ArxiuTar at = new ArxiuTar();
            try{
                // Assignam els bytes i la capçalera de l'arxiu
                capçaleraTar = Arrays.copyOfRange(arxiuSencer, byteActual, byteActual + blockSize);
                byteActual += blockSize;

                // Adjustam el tamany i assignam la capçalera de l'arxiu temporal
                this.adjustaTamany(capçaleraTar);
                at.setCapçalera(capçaleraTar);

                // Una vegada tinguem el tamany avançam el cursor
                arxiu = new byte[size];
                arxiu = Arrays.copyOfRange(arxiuSencer, byteActual, byteActual + size);
                // Incrementam el byte de l'arxiu
                byteActual += ompleTamany;
                at.setContingut(arxiu);

                // Cream una variable de tipus String per a emmagatzemar el nom de cada arxiu
                String nomArxiu = "";

                // Treiem el nom a partir dels bytes de la capçalera
                for (int i = 0; i < 100; i++) {
                    // Cream un byte temporal amb la mida de la capçalera a la posició de i
                    byte bt = capçaleraTar[i];

                    if(bt != 0){
                        nomArxiu += Character.toString((char) bt);
                    }else{
                        break;
                    }
                }

                at.setNomArxiu(nomArxiu);
                at.build();
                arxiusList.add(at);

            }catch (Exception e){
                System.out.println("He trobat tots els arxius del fitxer: " + locArxiu);
                totTrobat = true;
            }


        }
        this.carregat = true;

    }


}